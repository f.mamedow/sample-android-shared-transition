package fm.shared.transition.adapters

import android.animation.ValueAnimator
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.facebook.shimmer.Shimmer
import fm.shared.transition.R
import fm.shared.transition.databinding.ItemSliderBinding

class SliderAdapter(
    context: Context,
): RecyclerView.Adapter<SliderAdapter.ViewHolder>() {

    private val inflater = LayoutInflater.from(context)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemSliderBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = 10

    inner class ViewHolder(private val binding: ItemSliderBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind() {
            val context = binding.root.context
            val shimmer: Shimmer = Shimmer.ColorHighlightBuilder()
                .setBaseColor(context.resources.getColor(R.color.grey))
                .setHighlightColor(context.resources.getColor(R.color.white))
                .setRepeatMode(ValueAnimator.RESTART)
                .setDuration(2000)
                .setBaseAlpha(1f)
                .setHighlightAlpha(1f)
                .build()

            binding.root.setShimmer(shimmer)
            binding.root.showShimmer(true)
        }
    }
}