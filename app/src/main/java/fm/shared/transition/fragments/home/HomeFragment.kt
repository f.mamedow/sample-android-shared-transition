package fm.shared.transition.fragments.home

import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigatorExtras
import fm.shared.transition.R
import fm.shared.transition.adapters.SliderAdapter
import fm.shared.transition.databinding.FragmentHomeBinding
import fm.shared.transition.fragments.details.DetailsFragment

class HomeFragment : Fragment() {

    private var binding: FragmentHomeBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        binding = FragmentHomeBinding.bind(view)
        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setFragmentResultListener(REQUEST_LM_STATE_KEY) { requestKey, bundle ->
            when(requestKey) {
                REQUEST_LM_STATE_KEY -> {
                    val sliderState : Parcelable? = bundle.getParcelable(RESULT_LM_STATE_KEY)
                    binding?.rvSlider?.layoutManager?.onRestoreInstanceState(sliderState)
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupTransitions()
        setupView()
    }

    private fun setupView() {
        binding?.apply {
            rvSlider.adapter = SliderAdapter(requireContext())
            rvSlider.setCanTouch(false)
            rvSlider.isLoopEnabled = true

            tvDetails.setOnClickListener {
                val extras = FragmentNavigatorExtras(
                    tvTitle to DetailsFragment.TRANSITION_TITLE,
                    rvSlider to DetailsFragment.TRANSITION_SLIDER,
                    tvDetails to DetailsFragment.TRANSITION_DETAILS,
                    flSliderBg to DetailsFragment.TRANSITION_BACKGROUND,
                    tvDescriptionExpanded to DetailsFragment.TRANSITION_DESCRIPTION_EXPANDED,
                    tvDescriptionCollapsed to DetailsFragment.TRANSITION_DESCRIPTION_COLLAPSED,
                )

                val destination = HomeFragmentDirections.actionHomeToDetails(
                    layoutManagerState = rvSlider.layoutManager?.onSaveInstanceState()
                )
                it.findNavController().navigate(destination, extras)
            }
        }
    }

    private fun setupTransitions() {
        binding?.apply {
            ViewCompat.setTransitionName(tvTitle, DetailsFragment.TRANSITION_TITLE)
            ViewCompat.setTransitionName(rvSlider, DetailsFragment.TRANSITION_SLIDER)
            ViewCompat.setTransitionName(tvDescriptionExpanded, DetailsFragment.TRANSITION_DESCRIPTION_EXPANDED)
            ViewCompat.setTransitionName(tvDescriptionCollapsed, DetailsFragment.TRANSITION_DESCRIPTION_COLLAPSED)
            ViewCompat.setTransitionName(tvDetails, DetailsFragment.TRANSITION_DETAILS)
            ViewCompat.setTransitionName(flSliderBg, DetailsFragment.TRANSITION_BACKGROUND)
        }
    }


    override fun onPause() {
        super.onPause()
        binding?.rvSlider?.pauseAutoScroll(true)
    }

    override fun onResume() {
        super.onResume()
        binding?.rvSlider?.post {
            binding?.rvSlider?.startAutoScroll()
        }
    }

    override fun onDestroy() {
        binding = null
        super.onDestroy()
    }

    companion object {
        const val REQUEST_LM_STATE_KEY = "request_layoutmanager_state_key"
        const val RESULT_LM_STATE_KEY = "result_layoutmanager_state_key"
    }

}