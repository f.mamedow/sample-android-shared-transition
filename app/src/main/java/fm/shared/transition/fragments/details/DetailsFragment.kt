package fm.shared.transition.fragments.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.activity.addCallback
import androidx.core.os.bundleOf
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import fm.shared.transition.R
import fm.shared.transition.adapters.SliderAdapter
import fm.shared.transition.databinding.FragmentDetailsBinding
import fm.shared.transition.fragments.home.HomeFragment.Companion.REQUEST_LM_STATE_KEY
import fm.shared.transition.fragments.home.HomeFragment.Companion.RESULT_LM_STATE_KEY
import fm.shared.transition.utils.DetailsTransition

class DetailsFragment : Fragment() {

    private var binding: FragmentDetailsBinding? = null

    private val args: DetailsFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = DetailsTransition(2000)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_details, container, false)
        binding = FragmentDetailsBinding.bind(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        setupTransitions()
        setupBackButtonPressedDispatcher()

    }


    private fun setupView() {
        binding?.apply {
            rvSlider.adapter = SliderAdapter(requireContext())
            rvSlider.layoutManager?.onRestoreInstanceState(args.layoutManagerState)
            rvSlider.setCanTouch(false)
            rvSlider.isLoopEnabled = true
        }
    }

    private fun setupTransitions() {
        binding?.apply {
            ViewCompat.setTransitionName(tvTitle, TRANSITION_TITLE)
            ViewCompat.setTransitionName(rvSlider, TRANSITION_SLIDER)
            ViewCompat.setTransitionName(tvDescriptionExpanded, TRANSITION_DESCRIPTION_EXPANDED)
            ViewCompat.setTransitionName(tvDescriptionCollapsed, TRANSITION_DESCRIPTION_COLLAPSED)
            ViewCompat.setTransitionName(tvDetails, TRANSITION_DETAILS)
            ViewCompat.setTransitionName(flSliderBg, TRANSITION_BACKGROUND)
        }
    }

    private fun setupBackButtonPressedDispatcher() {
        requireActivity().onBackPressedDispatcher.addCallback(
            owner = viewLifecycleOwner,
            onBackPressed = {
                binding?.apply {
                    rvSlider.pauseAutoScroll(true)
                    rvSlider.layoutManager?.onSaveInstanceState()?.let {
                        setFragmentResult(REQUEST_LM_STATE_KEY, bundleOf(RESULT_LM_STATE_KEY to it))
                    }
                    root.findNavController().navigateUp()
                }
            }
        )
    }


    override fun onPause() {
        binding?.rvSlider?.pauseAutoScroll(true)
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        binding?.rvSlider?.startAutoScroll()
    }

    override fun onDestroy() {
        binding = null
        super.onDestroy()
    }


    companion object {
        const val TRANSITION_DESCRIPTION_EXPANDED = "tv_description_expanded_transition"
        const val TRANSITION_DESCRIPTION_COLLAPSED = "tv_description_collapsed_transition"
        const val TRANSITION_DETAILS = "tv_details_transition"
        const val TRANSITION_SLIDER = "rv_slider_transition"
        const val TRANSITION_TITLE = "title_transition"
        const val TRANSITION_BACKGROUND = "background_transition"
    }

}