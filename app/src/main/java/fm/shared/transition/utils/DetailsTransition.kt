package fm.shared.transition.utils

import androidx.transition.ChangeBounds
import androidx.transition.ChangeImageTransform
import androidx.transition.ChangeTransform
import androidx.transition.Explode
import androidx.transition.TransitionSet
import com.google.android.material.transition.Hold
import com.google.android.material.transition.MaterialElevationScale

class DetailsTransition(duration: Long) : TransitionSet() {
    init {
        setDuration(duration)
        ordering = ORDERING_TOGETHER
        addTransition(AlphaTransition())
        addTransition(MaterialElevationScale(true))
        addTransition(Hold())
        addTransition(Explode())
        addTransition(ChangeImageTransform())
        addTransition(ChangeBounds())
        addTransition(ChangeTransform())
    }
}