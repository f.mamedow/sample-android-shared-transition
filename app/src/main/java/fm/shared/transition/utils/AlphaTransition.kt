package fm.shared.transition.utils

import android.animation.Animator
import android.animation.ObjectAnimator
import android.util.Property
import android.view.View
import android.view.ViewGroup
import androidx.transition.Transition
import androidx.transition.TransitionValues

class AlphaTransition() : Transition() {


    override fun getTransitionProperties(): Array<String> {
        return TRANSITION_PROPERTIES.toTypedArray()
    }

    override fun captureStartValues(transitionValues: TransitionValues) {
        captureValue(transitionValues)
    }

    override fun captureEndValues(transitionValues: TransitionValues) {
        captureValue(transitionValues)
    }


    private fun captureValue(transitionValue: TransitionValues) {
        val targetView = transitionValue.view
        transitionValue.values[PROP_NAME_ALPHA] = targetView.alpha
    }

    override fun createAnimator(
        sceneRoot: ViewGroup,
        startValues: TransitionValues?,
        endValues: TransitionValues?
    ): Animator? {
        if (startValues == null || endValues == null) {
            return null
        }

        val startAlpha = startValues.values[PROP_NAME_ALPHA] as Float?
        val endAlpha = endValues.values[PROP_NAME_ALPHA] as Float?
        if (startAlpha == null || endAlpha == null || startAlpha == endAlpha) {
            return null
        }

        val targetView = endValues.view
        targetView.alpha = startAlpha
        return ObjectAnimator.ofFloat(targetView, AlphaProperty(), startAlpha, endAlpha)
    }


    private class AlphaProperty : Property<View, Float>(Float::class.java, "alpha") {
        override fun get(view: View): Float {
            return view.alpha
        }

        override fun set(view: View, value: Float) {
            view.alpha = value
        }
    }

    companion object {
        private const val PROP_NAME_ALPHA = "uralkali:transition:alpha"
        private val TRANSITION_PROPERTIES = listOf(PROP_NAME_ALPHA)
    }
}